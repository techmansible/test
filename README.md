# Ansible Role: PostgreSQL

[![Build Status](https://travis-ci.org/geerlingguy/ansible-role-postgresql.svg?branch=master)](https://travis-ci.org/geerlingguy/ansible-role-postgresql)

Installs and configures PostgreSQL server on RHEL/CentOS or Debian/Ubuntu servers.

## Playbook has been updated to install postgres-10(updated version) on RHEL servers

## Requirements

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

    - hosts: database
      roles:
        - role: geerlingguy.postgresql
          become: yes

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    postgresql_enablerepo: ""

(RHEL/CentOS only) You can set a repo to use for the PostgreSQL installation by passing it in here.

    postgresql_restarted_state: "restarted"

Set the state of the service when configuration changes are made. Recommended values are `restarted` or `reloaded`.

    postgresql_python_library: python-psycopg2

Library used by Ansible to communicate with PostgreSQL. If you are using Python 3 (e.g. set via `ansible_python_interpreter`), you should change this to `python3-psycopg2`.

    postgresql_user: postgres
    postgresql_group: postgres

The user and group under which PostgreSQL will run.

    postgresql_unix_socket_directories:
      - /var/run/postgresql

The directories (usually one, but can be multiple) where PostgreSQL's socket will be created.

    postgresql_global_config_options:
      - option: unix_socket_directories
        value: '{{ postgresql_unix_socket_directories | join(",") }}'

Global configuration options that will be set in `postgresql.conf`. Note that for RHEL/CentOS 6 (or very old versions of PostgreSQL), you need to at least override this variable and set the `option` to `unix_socket_directory`.

    postgresql_hba_entries:
      - { type: local, database: all, user: postgres, auth_method: peer }
      - { type: local, database: all, user: all, auth_method: peer }
      - { type: host, database: all, user: all, address: '127.0.0.1/32', auth_method: md5 }
      - { type: host, database: all, user: all, address: '::1/128', auth_method: md5 }

Configure [host based authentication](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html) entries to be set in the `pg_hba.conf`. Options for entries include:

  - `type` (required)
  - `database` (required)
  - `user` (required)
  - `address` (one of this or the following two are required)
  - `ip_address`
  - `ip_mask`
  - `auth_method` (required)
  - `auth_options` (optional)

If overriding, make sure you copy all of the existing entries from `defaults/main.yml` if you need to preserve existing entries.

    postgresql_locales:
      - 'en_US.UTF-8'

(Debian/Ubuntu only) Used to generate the locales used by PostgreSQL databases.

    postgresql_databases:
      - name: exampledb # required; the rest are optional
        lc_collate: # defaults to 'en_US.UTF-8'
        lc_ctype: # defaults to 'en_US.UTF-8'
        encoding: # defaults to 'UTF-8'
        template: # defaults to 'template0'
        login_host: # defaults to 'localhost'
        login_password: # defaults to not set
        login_user: # defaults to 'postgresql_user'
        login_unix_socket: # defaults to 1st of postgresql_unix_socket_directories
        port: # defaults to not set
        owner: # defaults to postgresql_user
        state: # defaults to 'present'

A list of databases to ensure exist on the server. Only the `name` is required; all other properties are optional.

    postgresql_users:
      - name: jdoe #required; the rest are optional
        password: # defaults to not set
        encrypted: # defaults to not set
        priv: # defaults to not set
        role_attr_flags: # defaults to not set
        db: # defaults to not set
        login_host: # defaults to 'localhost'
        login_password: # defaults to not set
        login_user: # defaults to '{{ postgresql_user }}'
        login_unix_socket: # defaults to 1st of postgresql_unix_socket_directories
        port: # defaults to not set
        state: # defaults to 'present'

A list of users to ensure exist on the server. Only the `name` is required; all other properties are optional.

    postgresql_version: [OS-specific]
    postgresql_data_dir: [OS-specific]
    postgresql_bin_path: [OS-specific]
    postgresql_config_path: [OS-specific]
    postgresql_daemon: [OS-specific]
    postgresql_packages: [OS-specific]

OS-specific variables that are set by include files in this role's `vars` directory. These shouldn't be overridden unless you're using a version of PostgreSQL that wasn't installed using system packages.

## Updated Variables

    bitbucket_url: "https://artifactory-new.pru.intranet.asia:8443/artifactory/generic_rt_mfp/armdevops/"
Artifactory URL where the postgres binary is present. 
If needed , postgres binaries can be uploaded to different directory by downloading it from the above mentioned path.

	bitbucket_artifactory_username: "kd00471859@techmahindra.com"
Artifactory username

	bitbucket_artifactory_password: "eyJ2ZXIiOiIyIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYiLCJraWQiOiJOZnFSY2tNOUJTLWJZck9FWmwyLV9FeDB5eTB6TGFpa2ZtOW5yTjE1NkQ0In0.eyJzdWIiOiJqZnJ0QDAxY3F3eW4wN2pnM3hhMThiaHluOG4xNmhiXC91c2Vyc1wva2QwMDQ3MTg1OUB0ZWNobWFoaW5kcmEuY29tIiwic2NwIjoibWVtYmVyLW9mLWdyb3Vwczpwcm9qZWN0LXJ0LW1mcCBhcGk6KiIsImF1ZCI6ImpmcnRAMDFjcXd5bjA3amczeGExOGJoeW44bjE2aGIiLCJpc3MiOiJqZnJ0QDAxY3F3eW4wN2pnM3hhMThiaHluOG4xNmhiIiwiaWF0IjoxNTQ5ODc5MjM2LCJqdGkiOiI2ZmY4ZTYzNS1hYmE2LTQxNjktYWRmOS1kNDUwODNhYzY5MDAifQ.NJwmF2QOFqehAiZ-JyMMq6YBRA_QNo7fFtphid5LzN5EPLU864GhFiLA0MBd8mZK2rtIjQ8Z4WfrPEg3K9CfUAO0Bm_8PVOxVcSmoAluSUZPycP5eWiGb7D5lstO8zcZY4gATFKxCK8OJCJ5lTl7wXDo93YDMhNnrWZzo50UsHErBrxMYLxQlz-8Tom1wQBPOUnRnCUwXU33AIBBG-IJ8VU0YgvdMB2aAWOOSbQYFRsn_sURFzpD7l0GSa95iHIy9iKRK-75rlRUN2JYmwEh6frHHmOHtibXL0_zL6QTwvYOwTGc4u9oVIhTGNcHZVBddQof-MU2m3NXSQbFrOSNLw"
Artifactory password

    postgres_binary_file: "pgdg-redhat10-10-2.noarch.rpm"
Name of the binary file

    postgres_binary_download_tmp_destination_directory: /var/tmp/postgres/
Temporary directory to download the binary

    postgres_service_file_directory: /usr/lib/systemd/system
Directory of postgresDB service file

    postgres_service_file_option:
      - option: Environment=PGDATA
        value: /opt/armdata/postgresql/10/data/
Variable of values updates in postgresDB service file

    postgres_destination_data_directory: /opt/armdata/postgresql/
Updated postgresDB data directory

    postgres_data_directory: /var/lib/pgsql/10
Data directory of postgresDB

    postgresql_global_config_options:
      - option: data_directory
        value: /opt/armdata/postgresql/10/data
Global configuration options that will be set in `postgresql.conf`

    postgresql_hba_entries:
      - { type: local, database: all, user: postgres, auth_method: peer }
      - { type: local, database: all, user: all, auth_method: peer }
      - { type: host, database: all, user: all, address: '127.0.0.1/32', auth_method: trust }
      - { type: host, database: all, user: all, address: '::1/128', auth_method: ident }

Configure [host based authentication](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html) entries to be set in the `pg_hba.conf`. Options for entries include:


## Dependencies

None.

## Example Playbook

    - hosts: database
      become: yes
      vars_files:
        - vars/main.yml
      roles:
        - geerlingguy.postgresql

*Inside `vars/main.yml`*:

    postgresql_databases:
      - name: example_db
    postgresql_users:
      - name: example_user
        password: supersecure

## License

MIT / BSD

## Author Information

This role was created in 2016 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/).
